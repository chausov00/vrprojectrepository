﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hint : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    #region Unity Editor

    [SerializeField] private List<GameObject> hints;

    #endregion

    #region IPointer Methods

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        int countOfHints = hints.Count;
        for (int i=0;i<countOfHints; i++)
        {
            hints[i].SetActive(true);
        }
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        int countOfHints = hints.Count;
        for (int i = 0; i < countOfHints; i++)
        {
            hints[i].SetActive(false);
        }
    }

    #endregion

}
