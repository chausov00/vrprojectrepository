﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeController : MonoBehaviour
{

    #region Enums

    public enum ModeGame
    {
        tour,
        quest
    }

    #endregion

    #region Private Fields

    private static ModeGame _currentGameMode;

    #endregion

    #region Public Methods

    public static void SetGameMode(ModeGame mode)
    {
        _currentGameMode = mode;
        Debug.Log("current mode: " + _currentGameMode.ToString());
    }

    public static ModeGame GetGameMode()
    {
        return _currentGameMode;
    }

    #endregion
}
