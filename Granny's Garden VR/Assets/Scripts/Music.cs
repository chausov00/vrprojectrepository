﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Music : MonoBehaviour
{
    public Slider volumeSounds;
    public Text volueCount;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Volume"))
        {
            volumeSounds.value = PlayerPrefs.GetFloat("Volume", volumeSounds.value);
        }
        else
        {
            volumeSounds = volumeSounds;
        }
        volueCount.text = volumeSounds.value.ToString();
    }

    public void SetVolume()
    {
        volueCount.text = volumeSounds.value.ToString();
        AudioListener.volume = volumeSounds.value;
        PlayerPrefs.SetFloat("Volume", volumeSounds.value);
        PlayerPrefs.Save();
    }

}
